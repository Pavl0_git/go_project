FROM ubuntu
ADD ../migrations/location ./migrations/location

COPY --from=builder /app/location/main ./location/app

EXPOSE ${LOCATION_HTTP_SERVER_PORT}

CMD ["./location/app"]