FROM ubuntu
ADD ../migrations/driver /migrations/driver

COPY --from=builder /app/driver/main ./driver/app

#EXPOSE ${DRIVER_HTTP_SERVER_PORT}

CMD ["./driver/app"]
