FROM golang:1.21-alpine as builder

RUN apk add --no-cache git

WORKDIR /app

COPY go.mod .
COPY go.sum .

RUN go mod download

COPY internal ./internal
COPY cmd ./cmd
COPY api ./api
COPY pkg ./pkg
COPY migrations ./migrations

#WORKDIR /cmd

RUN go build -o driver/main cmd/driver/main.go
RUN go build -o location/main cmd/location/main.go