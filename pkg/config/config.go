package config

import (
	"github.com/davecgh/go-spew/spew"
	"os"
	"strconv"
)

const (
	EnvProduction = "production"
	EnvPrefix     = "DEMO"
)

type Config struct {
	Environment string `env:"ENVIRONMENT" default:"development"`

	DriverServer     DriverServerConfig     `env:"-"`
	DriverMongo      DriverMongoConfig      `env:"-"`
	DriverMigrations DriverMigrationsConfig `env:"-"`

	LocationServer     LocationServerConfig     `env:"-"`
	LocationPostgres   LocationPostgresConfig   `env:"-"`
	LocationMigrations LocationMigrationsConfig `env:"-"`
}

type DriverServerConfig struct {
	HttpServerPort int `env:"DRIVER_HTTP_SERVER_PORT" default:"8080"`
}

type DriverMongoConfig struct {
	Database string `env:"DRIVER_MONGO_DATABASE" default:"driver"`
	Uri      string `env:"DRIVER_MONGO_URI"`
}

type DriverMigrationsConfig struct {
	URI     string `env:"DRIVER_MONGO_MIGRATION_URI"`
	Path    string `env:"DRIVER_MIGRATIONS_PATH"`
	Enabled string `env:"DRIVER_MIGRATIONS_ENABLED"   default:"false"`
}

type LocationServerConfig struct {
	HttpServerPort int `env:"LOCATION_HTTP_SERVER_PORT" default:"8081"`
}

type LocationPostgresConfig struct {
	Database string `env:"LOCATION_POSTGRES_DATABASE" default:"location"`
	DSN      string `env:"LOCATION_POSTGRES_DSN" default:"dsn://"`
}

type LocationMigrationsConfig struct {
	Path    string `env:"LOCATION_POSTGRES_PATH"`
	Enabled string `env:"LOCATION_POSTGRES_ENABLED"   default:"false"`
}

func NewConfig() (*Config, error) {
	driver_port, _ := strconv.Atoi(os.Getenv("DRIVER_HTTP_SERVER_PORT"))
	driver_server_config := DriverServerConfig{
		HttpServerPort: driver_port,
	}

	location_port, _ := strconv.Atoi(os.Getenv("LOCATION_HTTP_SERVER_PORT"))
	location_server_config := LocationServerConfig{
		HttpServerPort: location_port,
	}

	driver_mongo_config := DriverMongoConfig{
		Database: os.Getenv("DRIVER_MONGO_DATABASE"),
		Uri:      os.Getenv("DRIVER_MONGO_URI"),
	}

	location_postgres_config := LocationPostgresConfig{
		Database: os.Getenv("LOCATION_POSTGRES_DATABASE"),
		DSN:      os.Getenv("LOCATION_POSTGRES_DSN"),
	}

	driver_migrations_config := DriverMigrationsConfig{
		URI:     os.Getenv("DRIVER_MONGO_MIGRATION_URI"),
		Path:    os.Getenv("DRIVER_MIGRATIONS_PATH"),
		Enabled: os.Getenv("DRIVER_MIGRATIONS_ENABLED"),
	}

	location_migrations_config := LocationMigrationsConfig{
		Path:    os.Getenv("LOCATION_MIGRATIONS_PATH"),
		Enabled: os.Getenv("LOCATION_MIGRATIONS_ENABLED"),
	}

	config := Config{
		DriverServer:       driver_server_config,
		DriverMigrations:   driver_migrations_config,
		DriverMongo:        driver_mongo_config,
		LocationMigrations: location_migrations_config,
		LocationPostgres:   location_postgres_config,
		LocationServer:     location_server_config,
		Environment:        os.Getenv("ENVIRONMENT"),
	}

	if config.Environment != EnvProduction {
		spew.Dump(config)
	}

	return &config, nil
}
