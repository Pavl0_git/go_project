package migration

import (
	"errors"
	"fmt"
	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database/mongodb"
	"github.com/golang-migrate/migrate/v4/source/file"
	"go.mongodb.org/mongo-driver/mongo"
	"log/slog"
	"time"
)

const (
	disconnectTimeout = 30 * time.Second
)

type MigrationsService struct {
	logger *slog.Logger
	db     *mongo.Database
}

func NewMigrationsService(log *slog.Logger, db *mongo.Database) *MigrationsService {
	return &MigrationsService{
		logger: log,
		db:     db,
	}
}

func (m *MigrationsService) RunMigrations(path string) error {
	if path == "" {
		m.logger.Info("migration was skipped")
	}
	if m.db == nil {
		err := errors.New("run migration connect is not exists")
		return err
	}

	driver, err := mongodb.WithInstance(
		m.db.Client(),
		&mongodb.Config{DatabaseName: m.db.Name()},
	)
	if err != nil {
		return fmt.Errorf("cannot instantiate mongo driver: %w", err)
	}

	fsrc, err := (&file.File{}).Open(path)
	if err != nil {
		return fmt.Errorf("cannot open migration source: %w", err)
	}

	instance, err := migrate.NewWithInstance("file", fsrc, "mongo", driver)
	if err != nil {
		return fmt.Errorf("new migrate instance create error: %w", err)
	}

	if err := instance.Up(); err != nil && !errors.Is(err, migrate.ErrNoChange) {
		return fmt.Errorf("migration failed: instance.Up(): %w", err)
	}

	return nil
}
