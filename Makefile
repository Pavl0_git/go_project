include .env.dev
export

start:
	docker-compose --env-file .env.dev build
	docker-compose --env-file .env.dev up

test:
	go test -tags=containers ./internal/test/driver
	go test -tags=containers ./internal/test/location
