package model

type DriverTrip struct {
	TripID   string `bson:"trip_id"`
	DriverID string `bson:"driver_id"`
}
