package model

type Location struct {
	Latitude  float32 `json:"lat" bson:"lat"`
	Longitude float32 `json:"lng" bson:"lng"`
}

type Circle struct {
	Latitude  float32 `json:"lat"`
	Longitude float32 `json:"lng"`
	Radius    float32 `json:"radius"`
}
