package model

import "time"

type CancelData struct {
	TripID string `json:"trip_id"`
	Reason string `json:"reason"`
}

type AcceptData struct {
	TripID   string `json:"trip_id"`
	DriverID string `json:"driver_id"`
}

type Data struct {
	TripID string `json:"trip_id"`
}

type CancelCommand struct {
	ID              string     `json:"id"`
	Source          string     `json:"source"`
	Type            string     `json:"type"`
	DataContentType string     `json:"datacontenttype"`
	Time            time.Time  `json:"time"`
	Data            CancelData `json:"data"`
}

type AcceptCommand struct {
	ID              string     `json:"id"`
	Source          string     `json:"source"`
	Type            string     `json:"type"`
	DataContentType string     `json:"datacontenttype"`
	Time            time.Time  `json:"time"`
	Data            AcceptData `json:"data"`
}

type Command struct {
	ID              string    `json:"id"`
	Source          string    `json:"source"`
	Type            string    `json:"type"`
	DataContentType string    `json:"datacontenttype"`
	Time            time.Time `json:"time"`
	Data            Data      `json:"data"`
}
