package model

type Driver struct {
	DriverID  string  `json:"driver_id" db:"id"`
	Latitude  float32 `json:"lat" db:"lat"`
	Longitude float32 `json:"lng" db:"lng"`
}
