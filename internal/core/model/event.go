package model

import "time"

type CreatedData struct {
	TripID        string   `json:"trip_id"`
	OfferID       string   `json:"offer_id"`
	Price         Price    `json:"price"`
	Status        string   `json:"status"`
	StartLocation Location `json:"from"`
	FinalLocation Location `json:"to"`
}

type CreatedEvent struct {
	ID              string      `json:"id"`
	Source          string      `json:"source"`
	Type            string      `json:"type"`
	DataContentType string      `json:"datacontenttype"`
	Time            time.Time   `json:"time"`
	Data            CreatedData `json:"data"`
}

type CanceledData struct {
	TripID string `json:"trip_id"`
}

type CanceledEvent struct {
	ID              string       `json:"id"`
	Source          string       `json:"source"`
	Type            string       `json:"type"`
	DataContentType string       `json:"datacontenttype"`
	Time            time.Time    `json:"time"`
	Data            CanceledData `json:"data"`
}
