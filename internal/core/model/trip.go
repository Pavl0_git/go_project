package model

const (
	TripStatusCanceled = "CANCELED"
	TripStatusStarted  = "STARTED"
	TripStatusSearch   = "DRIVER_SEARCH"
	TripStatusEnded    = "ENDED"
	TripStatusWaiting  = "ON_POSITION"
	TripStatusFound    = "DRIVER_FOUND"
)

type Trip struct {
	TripID        string   `json:"id" bson:"trip_id"`
	DriverID      string   `json:"driver_id" bson:"driver_id"`
	StartLocation Location `json:"from" bson:"from"`
	FinalLocation Location `json:"to" bson:"to"`
	Price         Price    `json:"price" bson:"price"`
	Status        string   `json:"status" bson:"status"`
}
