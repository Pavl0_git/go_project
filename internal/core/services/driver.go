package services

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/segmentio/kafka-go"
	"io"
	"log/slog"
	"net/http"
	"taxi_services/internal/core/model"
	"taxi_services/internal/ports"
	"taxi_services/internal/ports/repository"
	"taxi_services/pkg/config"
	"time"
)

var _ ports.DriverSvc = (*DriverSvcImpl)(nil)

type KafkaInteractors struct {
	CancelWriter *kafka.Writer
	EndWriter    *kafka.Writer
	StartWriter  *kafka.Writer
	AcceptWriter *kafka.Writer
	CreateReader *kafka.Reader
	CancelReader *kafka.Reader
}

type PrometheusStat struct {
	TripQueueGauge     prometheus.Gauge
	CanceledTripsCount prometheus.Counter
}

type DriverSvcImpl struct {
	log              *slog.Logger
	kafkaInteractors KafkaInteractors
	driverRepo       repository.DriverRepo
	tripQueueRepo    repository.TripQueueRepo
	config           *config.Config
	prometheusStat   PrometheusStat
}

func NewDriverSvcImpl(log *slog.Logger, kafkaInteractors KafkaInteractors,
	driverRepo repository.DriverRepo, tripQueueRepo repository.TripQueueRepo, config *config.Config, reg *prometheus.Registry) *DriverSvcImpl {
	service := DriverSvcImpl{
		log:              log,
		kafkaInteractors: kafkaInteractors,
		driverRepo:       driverRepo,
		tripQueueRepo:    tripQueueRepo,
		config:           config,
		prometheusStat: PrometheusStat{
			TripQueueGauge: promauto.With(reg).NewGauge(prometheus.GaugeOpts{
				Namespace: "driver",
				Name:      "trip_queue_gauge",
				Help:      "Trip queue gauge"}),
			CanceledTripsCount: promauto.With(reg).NewCounter(prometheus.CounterOpts{
				Namespace: "driver",
				Name:      "canceled_trips_counter",
				Help:      "Canceled trips counter"}),
		},
	}
	if kafkaInteractors.CreateReader != nil {
		go func() {
			for {
				msg, err := kafkaInteractors.CreateReader.ReadMessage(context.Background())
				if err != nil {
					log.Error(err.Error())
					continue
				}
				event := model.CreatedEvent{}
				err = json.Unmarshal(msg.Value, &event)
				if err != nil {
					log.Error(err.Error())
					continue
				}

				err = service.InsertNewTrip(context.Background(), event)
				if err != nil {
					log.Error(err.Error())
					continue
				}

				time.Sleep(300 * time.Millisecond)
			}
		}()
	}
	if kafkaInteractors.CancelReader != nil {
		go func() {
			for {
				msg, err := kafkaInteractors.CancelReader.ReadMessage(context.Background())
				if err != nil {
					log.Error(err.Error())
					continue
				}
				event := model.CanceledEvent{}
				err = json.Unmarshal(msg.Value, &event)
				if err != nil {
					log.Error(err.Error())
					continue
				}

				err = service.ClientCancelTrip(context.Background(), event)
				if err != nil {
					log.Error(err.Error())
					continue
				}

				time.Sleep(300 * time.Millisecond)
			}
		}()
	}
	return &service
}

func (d DriverSvcImpl) InsertNewTrip(ctx context.Context, event model.CreatedEvent) error {
	data := event.Data

	trip := model.Trip{
		TripID:        data.TripID,
		StartLocation: data.StartLocation,
		FinalLocation: data.FinalLocation,
		Price:         data.Price,
		Status:        data.Status,
	}
	err := d.driverRepo.InsertTrip(ctx, trip)
	if err != nil {
		return fmt.Errorf("driverService.InsertNewTrip::%w", err)
	}
	d.prometheusStat.TripQueueGauge.Add(1)

	locationURL := fmt.Sprintf("http://0.0.0.0:%d", d.config.LocationServer.HttpServerPort)

	requestStr := fmt.Sprintf("%s/drivers?lat=%.6f&lng=%.6f&radius=%.6f", locationURL, trip.StartLocation.Latitude, trip.StartLocation.Longitude, 2000.0)

	resp, err := http.Get(requestStr)

	if err != nil {
		return fmt.Errorf("driverService.InsertNewTrip.Get::%w", err)
	}

	if resp.StatusCode == http.StatusNotFound {
		return errors.New("driverService.InsertNewTrip: got response 404 from location service")
	}

	respJson, _ := io.ReadAll(resp.Body)
	defer resp.Body.Close()

	var drivers []model.Driver

	err = json.Unmarshal(respJson, &drivers)
	if err != nil {
		return fmt.Errorf("driverService.InsertNewTrip.Unmarshall::%w", err)
	}

	var pairs []model.DriverTrip
	for _, driver := range drivers {
		pairs = append(pairs, model.DriverTrip{DriverID: driver.DriverID, TripID: trip.TripID})
	}

	err = d.tripQueueRepo.InsertPairs(ctx, pairs)
	if err != nil {
		return fmt.Errorf("driverService.InsertNewTrip.InsertPairs::%w", err)
	}
	return nil
}

func (d DriverSvcImpl) ClientCancelTrip(ctx context.Context, event model.CanceledEvent) error {
	tripID := event.Data.TripID
	err := d.driverRepo.ClientCancelTrip(ctx, tripID)
	if err != nil {
		return fmt.Errorf("driverService.ClientCancelTrip::%w", err)
	}
	d.prometheusStat.CanceledTripsCount.Add(1)
	err = d.tripQueueRepo.DeleteTrip(ctx, tripID)
	if err != nil {
		return fmt.Errorf("driverService.ClientCancelTrip.DeleteTrip::%w", err)
	}
	return nil
}

func (d DriverSvcImpl) GetTripForDriver(ctx context.Context, driverID string) ([]model.Trip, error) {
	tripsID, err := d.tripQueueRepo.GetTripForDriver(ctx, driverID)
	if err != nil {
		return nil, fmt.Errorf("driverService.GetTripForDriver::%w", err)
	}
	var res []model.Trip
	for _, tripID := range tripsID {
		trip, err := d.driverRepo.GetTrip(ctx, tripID)
		if err != nil {
			return nil, fmt.Errorf("driverService.GetTrip::%w", err)
		}
		res = append(res, trip)
	}
	return res, nil
}

func (d DriverSvcImpl) GetTripDriver(ctx context.Context, tripID string, driverID string) (model.Trip, error) {
	trip, err := d.driverRepo.GetTripDriver(ctx, tripID, driverID)
	if err != nil {
		return trip, fmt.Errorf("driverService.GetTripDriver::%w", err)
	}
	return trip, nil
}

// TODO: Change ID in kafka commands

func (d DriverSvcImpl) CancelTrip(ctx context.Context, tripID string, driverID string, reason string) error {
	err := d.driverRepo.DriverCancelTrip(ctx, tripID, driverID)
	if err != nil {
		return fmt.Errorf("driverService.CancelTrip.DriverCancelTrip::%w", err)
	}
	d.prometheusStat.CanceledTripsCount.Add(1)
	err = d.tripQueueRepo.DeleteTrip(ctx, tripID)
	if err != nil {
		return fmt.Errorf("driverService.CancelTrip.DeleteTrip::%w", err)
	}

	data := model.CancelData{TripID: tripID, Reason: reason}
	command := model.CancelCommand{
		ID:              "0",
		Source:          "/driver",
		Type:            "trip.command.cancel",
		DataContentType: "application/json",
		Time:            time.Now(),
		Data:            data,
	}
	jsonData, err := json.Marshal(command)
	if err != nil {
		d.log.Error(fmt.Errorf("driverService.CancelTrip.Marshall::%w", err).Error())
	} else {
		if d.kafkaInteractors.CancelWriter != nil {
			err = d.kafkaInteractors.CancelWriter.WriteMessages(ctx, kafka.Message{Value: jsonData})
			if err != nil {
				d.log.Error(fmt.Errorf("driverService.CancelTrip.WriteMessages::%w", err).Error())
			}
		}
	}
	return nil
}

func (d DriverSvcImpl) EndTrip(ctx context.Context, tripID string, driverID string) error {
	err := d.driverRepo.EndTrip(ctx, tripID, driverID)
	if err != nil {
		return fmt.Errorf("driverService.EndTrip::%w", err)
	}

	data := model.Data{TripID: tripID}
	command := model.Command{
		ID:              "0",
		Source:          "/driver",
		Type:            "trip.command.end",
		DataContentType: "application/json",
		Time:            time.Now(),
		Data:            data,
	}
	jsonData, err := json.Marshal(command)
	if err != nil {
		d.log.Error(fmt.Errorf("driverService.EndTrip.Marshall::%w", err).Error())
	} else {
		if d.kafkaInteractors.EndWriter != nil {
			err = d.kafkaInteractors.EndWriter.WriteMessages(ctx, kafka.Message{Value: jsonData})
			if err != nil {
				d.log.Error(fmt.Errorf("driverService.EndTrip.WriteMessages::%w", err).Error())
			}
		}
	}
	return nil
}

func (d DriverSvcImpl) StartTrip(ctx context.Context, tripID string, driverID string) error {
	err := d.driverRepo.StartTrip(ctx, tripID, driverID)
	if err != nil {
		return fmt.Errorf("driverService.StartTrip::%w", err)
	}

	data := model.Data{TripID: tripID}
	command := model.Command{
		ID:              "0",
		Source:          "/driver",
		Type:            "trip.command.start",
		DataContentType: "application/json",
		Time:            time.Now(),
		Data:            data,
	}
	jsonData, err := json.Marshal(command)
	if err != nil {
		d.log.Error(fmt.Errorf("driverService.StartTrip.Marshall::%w", err).Error())
	} else {
		if d.kafkaInteractors.StartWriter != nil {
			err = d.kafkaInteractors.StartWriter.WriteMessages(ctx, kafka.Message{Value: jsonData})
			if err != nil {
				d.log.Error(fmt.Errorf("driverService.StartTrip.WriteMessages::%w", err).Error())
			}
		}
	}
	return nil
}

func (d DriverSvcImpl) AcceptTrip(ctx context.Context, tripID string, driverID string) error {
	err := d.driverRepo.AcceptTrip(ctx, tripID, driverID)
	if err != nil {
		return fmt.Errorf("driverService.AcceptTrip::%w", err)
	}
	d.prometheusStat.TripQueueGauge.Sub(1)
	err = d.tripQueueRepo.DeleteTrip(ctx, tripID)
	if err != nil {
		d.log.Error(fmt.Errorf("driverService.AcceptTrip.DeleteTrip::%w", err).Error())
	}

	data := model.AcceptData{TripID: tripID, DriverID: driverID}
	command := model.AcceptCommand{
		ID:              "0",
		Source:          "/driver",
		Type:            "trip.command.accept",
		DataContentType: "application/json",
		Time:            time.Now(),
		Data:            data,
	}
	jsonData, err := json.Marshal(command)

	if err != nil {
		d.log.Error(fmt.Errorf("driverService.AcceptTrip.Marshall::%w", err).Error())
	} else {
		if d.kafkaInteractors.AcceptWriter != nil {
			err = d.kafkaInteractors.AcceptWriter.WriteMessages(ctx, kafka.Message{Value: jsonData})
			if err != nil {
				d.log.Error(fmt.Errorf("driverService.AcceptTrip.WriteMessages::%w", err).Error())
			}
		}
	}
	return nil
}

func (d DriverSvcImpl) Clear() error {
	err := d.driverRepo.Clear()
	if err != nil {
		return fmt.Errorf("driverService.Clear::%w", err)
	}
	err = d.tripQueueRepo.Clear()
	if err != nil {
		return fmt.Errorf("driverService.Clear::%w", err)
	}
	d.prometheusStat.TripQueueGauge.Set(0)
	return nil
}
