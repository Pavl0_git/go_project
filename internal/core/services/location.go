package services

import (
	"context"
	"fmt"
	"log/slog"
	"taxi_services/internal/core/model"
	"taxi_services/internal/ports"
	"taxi_services/internal/ports/repository"
)

var _ ports.LocationSvc = (*LocationSvcImpl)(nil)

type LocationSvcImpl struct {
	log          *slog.Logger
	locationRepo repository.LocationRepo
}

func NewLocationSvcImpl(log *slog.Logger, locationRepo repository.LocationRepo) *LocationSvcImpl {
	return &LocationSvcImpl{log: log, locationRepo: locationRepo}
}

func (l LocationSvcImpl) GetDrivers(ctx context.Context, location model.Location, radius float32) ([]model.Driver, error) {
	drivers, err := l.locationRepo.GetDrivers(ctx, location, radius)
	if err != nil {
		return nil, fmt.Errorf("locationService.GetDrivers::%w", err)
	}
	return drivers, nil
}

func (l LocationSvcImpl) ChangeDriverLocation(ctx context.Context, driverID string, location model.Location) error {
	err := l.locationRepo.ChangeDriverLocation(ctx, driverID, location)
	if err != nil {
		return fmt.Errorf("locationService.ChangeDriverLocation::%w", err)
	}
	return nil
}

func (l LocationSvcImpl) Clear() error {
	err := l.locationRepo.Clear()
	if err != nil {
		return fmt.Errorf("locationService.Clear::%w", err)
	}
	return nil
}
