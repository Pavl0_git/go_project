// Code generated by MockGen. DO NOT EDIT.
// Source: driver.go

// Package mock_ports is a generated GoMock package.
package mock_ports

import (
	context "context"
	reflect "reflect"
	model "taxi_services/internal/core/model"

	gomock "github.com/golang/mock/gomock"
)

// MockDriverSvc is a mock of DriverSvc interface.
type MockDriverSvc struct {
	ctrl     *gomock.Controller
	recorder *MockDriverSvcMockRecorder
}

// MockDriverSvcMockRecorder is the mock recorder for MockDriverSvc.
type MockDriverSvcMockRecorder struct {
	mock *MockDriverSvc
}

// NewMockDriverSvc creates a new mock instance.
func NewMockDriverSvc(ctrl *gomock.Controller) *MockDriverSvc {
	mock := &MockDriverSvc{ctrl: ctrl}
	mock.recorder = &MockDriverSvcMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockDriverSvc) EXPECT() *MockDriverSvcMockRecorder {
	return m.recorder
}

// AcceptTrip mocks base method.
func (m *MockDriverSvc) AcceptTrip(ctx context.Context, tripID, driverID string) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "AcceptTrip", ctx, tripID, driverID)
	ret0, _ := ret[0].(error)
	return ret0
}

// AcceptTrip indicates an expected call of AcceptTrip.
func (mr *MockDriverSvcMockRecorder) AcceptTrip(ctx, tripID, driverID interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "AcceptTrip", reflect.TypeOf((*MockDriverSvc)(nil).AcceptTrip), ctx, tripID, driverID)
}

// CancelTrip mocks base method.
func (m *MockDriverSvc) CancelTrip(ctx context.Context, tripID, driverID, reason string) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "CancelTrip", ctx, tripID, driverID, reason)
	ret0, _ := ret[0].(error)
	return ret0
}

// CancelTrip indicates an expected call of CancelTrip.
func (mr *MockDriverSvcMockRecorder) CancelTrip(ctx, tripID, driverID, reason interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "CancelTrip", reflect.TypeOf((*MockDriverSvc)(nil).CancelTrip), ctx, tripID, driverID, reason)
}

// EndTrip mocks base method.
func (m *MockDriverSvc) EndTrip(ctx context.Context, tripID, driverID string) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "EndTrip", ctx, tripID, driverID)
	ret0, _ := ret[0].(error)
	return ret0
}

// EndTrip indicates an expected call of EndTrip.
func (mr *MockDriverSvcMockRecorder) EndTrip(ctx, tripID, driverID interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "EndTrip", reflect.TypeOf((*MockDriverSvc)(nil).EndTrip), ctx, tripID, driverID)
}

// GetTripDriver mocks base method.
func (m *MockDriverSvc) GetTripDriver(ctx context.Context, tripID, driverID string) (model.Trip, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetTripDriver", ctx, tripID, driverID)
	ret0, _ := ret[0].(model.Trip)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetTripDriver indicates an expected call of GetTripDriver.
func (mr *MockDriverSvcMockRecorder) GetTripDriver(ctx, tripID, driverID interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetTripDriver", reflect.TypeOf((*MockDriverSvc)(nil).GetTripDriver), ctx, tripID, driverID)
}

// GetTripForDriver mocks base method.
func (m *MockDriverSvc) GetTripForDriver(ctx context.Context, driverID string) ([]model.Trip, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetTripForDriver", ctx, driverID)
	ret0, _ := ret[0].([]model.Trip)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetTripForDriver indicates an expected call of GetTripForDriver.
func (mr *MockDriverSvcMockRecorder) GetTripForDriver(ctx, driverID interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetTripForDriver", reflect.TypeOf((*MockDriverSvc)(nil).GetTripForDriver), ctx, driverID)
}

// StartTrip mocks base method.
func (m *MockDriverSvc) StartTrip(ctx context.Context, tripID, driverID string) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "StartTrip", ctx, tripID, driverID)
	ret0, _ := ret[0].(error)
	return ret0
}

// StartTrip indicates an expected call of StartTrip.
func (mr *MockDriverSvcMockRecorder) StartTrip(ctx, tripID, driverID interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "StartTrip", reflect.TypeOf((*MockDriverSvc)(nil).StartTrip), ctx, tripID, driverID)
}
