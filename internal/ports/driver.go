//go:generate mockgen -source=driver.go -destination=mocks/driver_mock.go
package ports

import (
	"context"
	"taxi_services/internal/core/model"
)

type DriverSvc interface {
	GetTripForDriver(ctx context.Context, driverID string) ([]model.Trip, error)
	GetTripDriver(ctx context.Context, tripID string, driverID string) (model.Trip, error)
	CancelTrip(ctx context.Context, tripID string, driverID string, reason string) error
	AcceptTrip(ctx context.Context, tripID string, driverID string) error
	StartTrip(ctx context.Context, tripID string, driverID string) error
	EndTrip(ctx context.Context, tripID string, driverID string) error
	InsertNewTrip(ctx context.Context, event model.CreatedEvent) error
	Clear() error
	ClientCancelTrip(ctx context.Context, event model.CanceledEvent) error
}
