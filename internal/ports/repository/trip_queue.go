//go:generate mockgen -source=trip_queue.go -destination=mocks/trip_queue_mock.go
package repository

import (
	"context"
	"taxi_services/internal/core/model"
)

type TripQueueRepo interface {
	GetTripForDriver(ctx context.Context, driverID string) ([]string, error)
	DeleteTrip(ctx context.Context, tripID string) error
	InsertPairs(ctx context.Context, pairs []model.DriverTrip) error
	Clear() error
}
