//go:generate mockgen -source=driver.go -destination=mocks/driver_mock.go
package repository

import (
	"context"
	"taxi_services/internal/core/model"
)

type DriverRepo interface {
	InsertTrip(ctx context.Context, trip model.Trip) error
	GetTrip(ctx context.Context, tripID string) (model.Trip, error)
	GetTripDriver(ctx context.Context, tripID string, driverID string) (model.Trip, error)
	DriverCancelTrip(ctx context.Context, tripID string, driverID string) error
	ClientCancelTrip(ctx context.Context, tripID string) error
	StartTrip(ctx context.Context, tripID string, driverID string) error
	AcceptTrip(ctx context.Context, tripID string, driverID string) error
	EndTrip(ctx context.Context, tripID string, driverID string) error
	Clear() error
}
