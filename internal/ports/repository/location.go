//go:generate mockgen -source=location.go -destination=mocks/location_mock.go
package repository

import (
	"context"
	"taxi_services/internal/core/model"
)

type LocationRepo interface {
	GetDrivers(ctx context.Context, location model.Location, radius float32) ([]model.Driver, error)
	ChangeDriverLocation(ctx context.Context, driverID string, location model.Location) error
	Clear() error
}
