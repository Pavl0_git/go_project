package handler

import (
	"encoding/json"
	"fmt"
	openapiTypes "github.com/oapi-codegen/runtime/types"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"io/ioutil"
	"log/slog"
	"net/http"
	openapiLocation "taxi_services/api/openapi/location"
	"taxi_services/internal/adapter/rest/response"
	"taxi_services/internal/core/model"
	otel "taxi_services/internal/otel"
	"taxi_services/internal/ports"
)

var (
	_ openapiLocation.ServerInterface = &LocationAPIImpl{}
)

type PrometheusLocationStat struct {
	getDriversCounter     prometheus.Counter
	updateLocationCounter prometheus.Counter
}

type LocationAPIImpl struct {
	serviceContainer ports.LocationSvc
	logger           *slog.Logger
	Response         *response.LocationResponse
	prometheusStat   PrometheusLocationStat
}

func (l LocationAPIImpl) GetDrivers(w http.ResponseWriter, r *http.Request, params openapiLocation.GetDriversParams) {
	l.prometheusStat.getDriversCounter.Inc()
	ctx := r.Context()

	_, span := otel.Tracer.Start(ctx, "get drivers")
	defer span.End()

	location := model.Location{Latitude: params.Lat, Longitude: params.Lng}

	drivers, err := l.serviceContainer.GetDrivers(ctx, location, params.Radius)
	if err != nil {
		err = fmt.Errorf("handler.GetDrivers.SvcGetDrivers: %w", err)
		l.logger.Error(err.Error())
		l.Response.WErrorResponse(err, w)
		return
	}
	l.Response.WJsonResponse(drivers, w, http.StatusOK)
}

func (l LocationAPIImpl) UpdateDriverLocation(w http.ResponseWriter, r *http.Request, driverId openapiTypes.UUID) {
	l.prometheusStat.updateLocationCounter.Inc()

	ctx := r.Context()
	_, span := otel.Tracer.Start(ctx, "Update driver location")
	defer span.End()
	req, err := ioutil.ReadAll(r.Body)

	if err != nil {
		err = fmt.Errorf("handler.UpdateDriverLocation.RequestRead: %w", err)
		l.logger.Error(err.Error())
		l.Response.WErrorResponse(err, w)
		return
	}

	var location model.Location
	err = json.Unmarshal(req, &location)
	if err != nil {
		err = fmt.Errorf("handler.UpdateDriverLocation.RequestUnmarshall: %w", err)
		l.logger.Error(err.Error())
		l.Response.WErrorResponse(err, w)
		return
	}

	driverID, err := driverId.MarshalText()
	if err != nil {
		err = fmt.Errorf("handler.UpdateDriverLocation.UnmarshallDriverID: %w", err)
		l.logger.Error(err.Error())
		l.Response.WErrorResponse(err, w)
		return
	}

	err = l.serviceContainer.ChangeDriverLocation(ctx, string(driverID), location)
	if err != nil {
		err = fmt.Errorf("handler.UpdateDriverLocation.SvcChangeDriverLocation: %w", err)
		l.logger.Error(err.Error())
		l.Response.WErrorResponse(err, w)
		return
	}
	l.Response.WJsonResponse(nil, w, http.StatusOK)
}

func NewLocationAPIImpl(
	logger *slog.Logger,
	serviceRegistry ports.LocationSvc,
	resp *response.LocationResponse,
) openapiLocation.ServerInterface {
	reg := prometheus.NewRegistry()
	return &LocationAPIImpl{
		serviceContainer: serviceRegistry,
		logger:           logger,
		Response:         resp,
		prometheusStat: PrometheusLocationStat{
			getDriversCounter: promauto.With(reg).NewCounter(prometheus.CounterOpts{
				Namespace: "location",
				Name:      "get_drivers_counter",
				Help:      "Get drivers counter"}),

			updateLocationCounter: promauto.With(reg).NewCounter(prometheus.CounterOpts{
				Namespace: "location",
				Name:      "update_location_counter",
				Help:      "Update location requests counter"}),
		},
	}
}
