package handler

import (
	"fmt"
	openapiTypes "github.com/oapi-codegen/runtime/types"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"log/slog"
	"net/http"
	openapiDriver "taxi_services/api/openapi/driver"
	"taxi_services/internal/adapter/rest/response"
	"taxi_services/internal/core/model"
	otel "taxi_services/internal/otel"
	"taxi_services/internal/ports"
)

var (
	_ openapiDriver.ServerInterface = &DriverAPIImpl{}
)

type PrometheusDriverStat struct {
	GetTripsCounter    prometheus.Counter
	GetTripByIDCounter prometheus.Counter
	AcceptTripCounter  prometheus.Counter
	CancelTripCounter  prometheus.Counter
	EndTripCounter     prometheus.Counter
	StartTripCounter   prometheus.Counter
}

type DriverAPIImpl struct {
	serviceContainer ports.DriverSvc
	logger           *slog.Logger
	Response         *response.DriverResponse
	prometheusStat   PrometheusDriverStat
}

func NewDriverAPIImpl(
	logger *slog.Logger,
	serviceRegistry ports.DriverSvc,
	resp *response.DriverResponse,
	reg *prometheus.Registry,
) openapiDriver.ServerInterface {

	return &DriverAPIImpl{
		serviceContainer: serviceRegistry,
		logger:           logger,
		Response:         resp,
		prometheusStat: PrometheusDriverStat{
			GetTripsCounter: promauto.With(reg).NewCounter(prometheus.CounterOpts{
				Namespace: "driver",
				Name:      "get_trips_counter",
				Help:      "Get trips counter"}),
			GetTripByIDCounter: promauto.With(reg).NewCounter(prometheus.CounterOpts{
				Namespace: "driver",
				Name:      "get_trip_by_id_counter",
				Help:      "Get trips by ID counter"}),
			AcceptTripCounter: promauto.With(reg).NewCounter(prometheus.CounterOpts{
				Namespace: "driver",
				Name:      "accept_trip_counter",
				Help:      "Accept trip counter"}),
			CancelTripCounter: promauto.With(reg).NewCounter(prometheus.CounterOpts{
				Namespace: "driver",
				Name:      "cancel_trip_counter",
				Help:      "Cancel trip counter"}),
			EndTripCounter: promauto.With(reg).NewCounter(prometheus.CounterOpts{
				Namespace: "driver",
				Name:      "end_trip_counter",
				Help:      "End trip counter"}),
			StartTripCounter: promauto.With(reg).NewCounter(prometheus.CounterOpts{
				Namespace: "driver",
				Name:      "start_trip_counter",
				Help:      "Start trip counter"}),
		},
	}
}

func (d DriverAPIImpl) GetTrips(w http.ResponseWriter, r *http.Request, params openapiDriver.GetTripsParams) {
	d.prometheusStat.GetTripsCounter.Inc()

	ctx := r.Context()

	_, span := otel.Tracer.Start(ctx, "get trips")
	defer span.End()

	driverID, err := params.UserId.MarshalText()

	if err != nil {
		err = fmt.Errorf("handler.GetTrips.UnmarshallDriverID: %w", err)
		d.logger.Error(err.Error())
		d.Response.WErrorResponse(err, w)
		return
	}

	var trips []model.Trip
	trips, err = d.serviceContainer.GetTripForDriver(ctx, string(driverID))
	if err != nil {
		err = fmt.Errorf("handler.GetTrips.SvcGetTripForDriver: %w", err)
		d.logger.Error(err.Error())
		d.Response.WErrorResponse(err, w)
		return
	}
	d.Response.WJsonResponse(trips, w, http.StatusOK)
}

func (d DriverAPIImpl) GetTripByID(w http.ResponseWriter, r *http.Request, tripId openapiTypes.UUID, params openapiDriver.GetTripByIDParams) {
	d.prometheusStat.GetTripByIDCounter.Inc()

	ctx := r.Context()

	_, span := otel.Tracer.Start(ctx, "get trip by id")
	defer span.End()

	driverID, err := params.UserId.MarshalText()
	if err != nil {
		err = fmt.Errorf("handler.GetTripByID.UnmarshallDriverID: %w", err)
		d.logger.Error(err.Error())
		d.Response.WErrorResponse(err, w)
		return
	}

	tripID, err := tripId.MarshalText()
	if err != nil {
		err = fmt.Errorf("handler.GetTripByID.UnmarshallTripID: %w", err)
		d.logger.Error(err.Error())
		d.Response.WErrorResponse(err, w)
		return
	}

	trip, err := d.serviceContainer.GetTripDriver(ctx, string(tripID), string(driverID))
	if err != nil {
		err = fmt.Errorf("handler.GetTripByID.SvcGetTripDriver: %w", err)
		d.logger.Error(err.Error())
		d.Response.WErrorResponse(err, w)
		return
	}
	d.Response.WJsonResponse(trip, w, http.StatusOK)
}

func (d DriverAPIImpl) AcceptTrip(w http.ResponseWriter, r *http.Request, tripId openapiTypes.UUID, params openapiDriver.AcceptTripParams) {
	d.prometheusStat.AcceptTripCounter.Inc()

	ctx := r.Context()

	_, span := otel.Tracer.Start(ctx, "accept trip")
	defer span.End()

	driverID, err := params.UserId.MarshalText()
	if err != nil {
		err = fmt.Errorf("handler.AcceptTrip.UnmarshallDriverID: %w", err)
		d.logger.Error(err.Error())
		d.Response.WErrorResponse(err, w)
		return
	}

	tripID, err := tripId.MarshalText()
	if err != nil {
		err = fmt.Errorf("handler.AcceptTrip.UnmarshallTripID: %w", err)
		d.logger.Error(err.Error())
		d.Response.WErrorResponse(err, w)
		return
	}

	err = d.serviceContainer.AcceptTrip(ctx, string(tripID), string(driverID))
	if err != nil {
		err = fmt.Errorf("handler.AcceptTrip.SvcAcceptTrip: %w", err)
		d.logger.With(err).Error(err.Error())
		d.Response.WErrorResponse(err, w)
		return
	}
	d.Response.WJsonResponse(nil, w, http.StatusOK)
}

func (d DriverAPIImpl) CancelTrip(w http.ResponseWriter, r *http.Request, tripId openapiTypes.UUID, params openapiDriver.CancelTripParams) {
	d.prometheusStat.CancelTripCounter.Inc()

	ctx := r.Context()

	_, span := otel.Tracer.Start(ctx, "cancel trip")
	defer span.End()

	driverID, err := params.UserId.MarshalText()
	if err != nil {
		err = fmt.Errorf("handler.CancelTrip.UnmarshallDriverID: %w", err)
		d.logger.Error(err.Error())
		d.Response.WErrorResponse(err, w)
		return
	}

	tripID, err := tripId.MarshalText()
	if err != nil {
		err = fmt.Errorf("handler.CancelTrip.UnmarshallTripID: %w", err)
		d.logger.Error(err.Error())
		d.Response.WErrorResponse(err, w)
		return
	}

	err = d.serviceContainer.CancelTrip(ctx, string(tripID), string(driverID), *params.Reason)
	if err != nil {
		err = fmt.Errorf("handler.CancelTrip.SvcCancelTrip: %w", err)
		d.logger.Error(err.Error())
		d.Response.WErrorResponse(err, w)
		return
	}
	d.Response.WJsonResponse(nil, w, http.StatusOK)
}

func (d DriverAPIImpl) EndTrip(w http.ResponseWriter, r *http.Request, tripId openapiTypes.UUID, params openapiDriver.EndTripParams) {
	d.prometheusStat.EndTripCounter.Inc()

	ctx := r.Context()

	_, span := otel.Tracer.Start(ctx, "end trip")
	defer span.End()

	driverID, err := params.UserId.MarshalText()
	if err != nil {
		err = fmt.Errorf("handler.EndTrip.UnmarshallDriverID: %w", err)
		d.logger.Error(err.Error())
		d.Response.WErrorResponse(err, w)
		return
	}

	tripID, err := tripId.MarshalText()
	if err != nil {
		err = fmt.Errorf("handler.EndTrip.UnmarshallTripID: %w", err)
		d.logger.Error(err.Error())
		d.Response.WErrorResponse(err, w)
		return
	}

	err = d.serviceContainer.EndTrip(ctx, string(tripID), string(driverID))
	if err != nil {
		err = fmt.Errorf("handler.EndTrip.SvcEndTrip: %w", err)
		d.logger.Error(err.Error())
		d.Response.WErrorResponse(err, w)
		return
	}
	d.Response.WJsonResponse(nil, w, http.StatusOK)
}

func (d DriverAPIImpl) StartTrip(w http.ResponseWriter, r *http.Request, tripId openapiTypes.UUID, params openapiDriver.StartTripParams) {
	d.prometheusStat.StartTripCounter.Inc()

	ctx := r.Context()

	_, span := otel.Tracer.Start(ctx, "start trip")
	defer span.End()

	driverID, err := params.UserId.MarshalText()
	if err != nil {
		err = fmt.Errorf("handler.StartTrip.UnmarshallDriverID: %w", err)
		d.logger.Error(err.Error())
		d.Response.WErrorResponse(err, w)
		return
	}

	tripID, err := tripId.MarshalText()
	if err != nil {
		err = fmt.Errorf("handler.StartTrip.UnmarshallTripID: %w", err)
		d.logger.Error(err.Error())
		d.Response.WErrorResponse(err, w)
		return
	}

	err = d.serviceContainer.StartTrip(ctx, string(tripID), string(driverID))
	if err != nil {
		err = fmt.Errorf("handler.StartTrip.SvcStartTrip: %w", err)
		d.logger.Error(err.Error())
		d.Response.WErrorResponse(err, w)
		return
	}
	d.Response.WJsonResponse(nil, w, http.StatusOK)
}
