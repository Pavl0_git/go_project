package rest

import (
	"context"
	"fmt"
	"github.com/go-chi/chi/v5"
	"log/slog"
	"net/http"
	openapiLocation "taxi_services/api/openapi/location"
	"taxi_services/internal/adapter/rest/handler"
	"taxi_services/internal/adapter/rest/middleware"
	"taxi_services/internal/adapter/rest/response"
	"taxi_services/internal/ports"
	"taxi_services/pkg/config"
)

type LocationServer struct {
	httpServer        *http.Server
	config            *config.Config
	locationContainer ports.LocationSvc
	logger            *slog.Logger
	router            chi.Router
}

func NewLocationServer(
	logger *slog.Logger,
	cfg *config.Config,
	locationContainer ports.LocationSvc,
) *LocationServer {
	server := &LocationServer{
		logger:            logger,
		config:            cfg,
		locationContainer: locationContainer,
	}

	return server
}

func (s *LocationServer) SetUp() {
	s.router = chi.NewRouter()
	s.router.Use(middleware.NewLogger(s.logger))
	responser := response.NewLocationResponse(s.logger)

	locationApi := handler.NewLocationAPIImpl(s.logger, s.locationContainer, responser)
	locationRouter := openapiLocation.HandlerWithOptions(locationApi, openapiLocation.ChiServerOptions{
		ErrorHandlerFunc: responser.ErrorHandlerFunc,
	})
	s.router.Use()
	s.router.Mount("/", locationRouter)

	s.httpServer = &http.Server{
		ReadTimeout:       readTimeout,
		WriteTimeout:      writeTimeout,
		IdleTimeout:       idleTimeout,
		ReadHeaderTimeout: readHeaderTimeout,
		Addr:              fmt.Sprintf("0.0.0.0:%d", s.config.LocationServer.HttpServerPort),
		Handler:           s.router,
	}
}

func (s *LocationServer) Start() error {
	s.logger.Info(fmt.Sprintf("http server starts on port: %d", s.config.LocationServer.HttpServerPort))
	return s.httpServer.ListenAndServe()
}

func (s *LocationServer) Stop(ctx context.Context) error {
	s.logger.Info(fmt.Sprintf("http server stops on port: %d", s.config.LocationServer.HttpServerPort))
	return s.httpServer.Shutdown(ctx)
}
