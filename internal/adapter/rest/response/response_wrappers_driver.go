package response

import (
	"encoding/json"
	"fmt"
	"log/slog"
	"net/http"
	"strings"
	openapiDriver "taxi_services/api/openapi/driver"
)

type DriverResponse struct {
	logger *slog.Logger
}

func NewDriverResponse(
	log *slog.Logger,
) *DriverResponse {
	return &DriverResponse{
		logger: log,
	}
}

func (s *DriverResponse) WJsonResponse(response interface{}, w http.ResponseWriter, code int) {
	var jsonData []byte

	if response != nil && response != http.NoBody {
		w.Header().Set("Accept", "application/json")

		var err error
		w.Header().Set("Content-Type", "application/json; charset=utf-8")
		jsonData, err = json.Marshal(response)
		if err != nil {
			s.logger.Error(fmt.Sprintf("reponse marshall: %s", err.Error()))
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
	}

	s.wResponse(jsonData, w, code)
}

func (s *DriverResponse) WErrorResponse(err error, w http.ResponseWriter) {
	switch {
	case strings.Contains(err.Error(), "incorrect"), strings.Contains(err.Error(), "invalid"):
		s.WJsonResponse(nil, w, http.StatusBadRequest)
	case strings.Contains(err.Error(), "found"):
		s.WJsonResponse(nil, w, http.StatusNotFound)
	default:
		s.WJsonResponse(nil, w, http.StatusInternalServerError)
	}
}

func (s *DriverResponse) wResponse(resp []byte, w http.ResponseWriter, code int) {
	w.WriteHeader(code)

	if len(resp) > 0 {
		_, err := w.Write(resp)
		if err != nil {
			s.logger.Error(err.Error())
		}
	}
}

//nolint:errorlint
func (s *DriverResponse) ErrorHandlerFunc(w http.ResponseWriter, r *http.Request, err error) {
	s.logger.Error(err.Error())
	switch err.(type) {
	case *openapiDriver.RequiredParamError,
		*openapiDriver.RequiredHeaderError,
		*openapiDriver.InvalidParamFormatError,
		*openapiDriver.TooManyValuesForParamError,
		*openapiDriver.UnescapedCookieParamError:
		s.WJsonResponse(nil, w, http.StatusBadRequest)
	default:
		s.WJsonResponse(nil, w, http.StatusInternalServerError)
	}
}
