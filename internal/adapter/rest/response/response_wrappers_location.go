package response

import (
	"encoding/json"
	"fmt"
	"log/slog"
	"net/http"
	"strings"
	openapiLocation "taxi_services/api/openapi/location"
)

type LocationResponse struct {
	logger *slog.Logger
}

func NewLocationResponse(
	log *slog.Logger,
) *LocationResponse {
	return &LocationResponse{
		logger: log,
	}
}

func (s *LocationResponse) WJsonResponse(response interface{}, w http.ResponseWriter, code int) {
	var jsonData []byte
	if response != nil && response != http.NoBody {
		w.Header().Set("Accept", "application/json")

		var err error
		w.Header().Set("Content-Type", "application/json; charset=utf-8")
		jsonData, err = json.Marshal(response)
		if err != nil {
			s.logger.Error(fmt.Sprintf("reponse marshall: %s", err.Error()))
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
	}

	s.wResponse(jsonData, w, code)
}

func (s *LocationResponse) WErrorResponse(err error, w http.ResponseWriter) {
	switch {
	case strings.Contains(err.Error(), "incorrect"), strings.Contains(err.Error(), "invalid"):
		s.WJsonResponse(nil, w, http.StatusBadRequest)
	case strings.Contains(err.Error(), "found"):
		s.WJsonResponse(nil, w, http.StatusNotFound)
	default:
		s.WJsonResponse(nil, w, http.StatusInternalServerError)
	}
}

func (s *LocationResponse) wResponse(resp []byte, w http.ResponseWriter, code int) {
	w.WriteHeader(code)

	if len(resp) > 0 {
		_, err := w.Write(resp)
		if err != nil {
			s.logger.Error(err.Error())
		}
	}
}

//nolint:errorlint
func (s *LocationResponse) ErrorHandlerFunc(w http.ResponseWriter, r *http.Request, err error) {
	s.logger.Error(err.Error())
	switch err.(type) {
	case *openapiLocation.RequiredParamError,
		*openapiLocation.RequiredHeaderError,
		*openapiLocation.InvalidParamFormatError,
		*openapiLocation.TooManyValuesForParamError,
		*openapiLocation.UnescapedCookieParamError:
		s.WJsonResponse(nil, w, http.StatusBadRequest)
	default:
		s.WJsonResponse(nil, w, http.StatusInternalServerError)
	}
}
