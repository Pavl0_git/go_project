package rest

import (
	"context"
	"fmt"
	"github.com/go-chi/chi/v5"
	"github.com/prometheus/client_golang/prometheus"
	"log/slog"
	"net/http"
	openapiDriver "taxi_services/api/openapi/driver"
	"taxi_services/internal/adapter/rest/handler"
	"taxi_services/internal/adapter/rest/middleware"
	"taxi_services/internal/adapter/rest/response"
	"taxi_services/internal/ports"
	"taxi_services/pkg/config"
	"time"
)

const (
	readTimeout       = 1 * time.Second
	writeTimeout      = 1 * time.Second
	idleTimeout       = 30 * time.Second
	readHeaderTimeout = 2 * time.Second
)

type DriverServer struct {
	httpServer      *http.Server
	config          *config.Config
	driverContainer ports.DriverSvc
	logger          *slog.Logger
	router          chi.Router
	reg             *prometheus.Registry
}

func NewDriverServer(
	logger *slog.Logger,
	cfg *config.Config,
	driverContainer ports.DriverSvc,
	reg *prometheus.Registry,
) *DriverServer {
	server := &DriverServer{
		logger:          logger,
		config:          cfg,
		driverContainer: driverContainer,
		reg:             reg,
	}

	return server
}

func (s *DriverServer) SetUp() {
	s.router = chi.NewRouter()
	s.router.Use(middleware.NewLogger(s.logger))

	responser := response.NewDriverResponse(s.logger)

	driverApi := handler.NewDriverAPIImpl(s.logger, s.driverContainer, responser, s.reg)
	driverRouter := openapiDriver.HandlerWithOptions(driverApi, openapiDriver.ChiServerOptions{
		ErrorHandlerFunc: responser.ErrorHandlerFunc,
	})
	s.router.Use()
	s.router.Mount("/", driverRouter)

	s.httpServer = &http.Server{
		ReadTimeout:       readTimeout,
		WriteTimeout:      writeTimeout,
		IdleTimeout:       idleTimeout,
		ReadHeaderTimeout: readHeaderTimeout,
		Addr:              fmt.Sprintf("0.0.0.0:%d", s.config.DriverServer.HttpServerPort),
		Handler:           s.router,
	}
}

func (s *DriverServer) Start() error {
	s.logger.Info(fmt.Sprintf("http server starts on port: %d", s.config.DriverServer.HttpServerPort))
	return s.httpServer.ListenAndServe()
}

func (s *DriverServer) Stop(ctx context.Context) error {
	s.logger.Info(fmt.Sprintf("http server stops on port: %d", s.config.DriverServer.HttpServerPort))
	return s.httpServer.Shutdown(ctx)
}
