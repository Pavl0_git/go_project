package db

import (
	"context"
	"errors"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"log/slog"
	"taxi_services/internal/core/model"
	"taxi_services/internal/ports/repository"
	"time"
)

const (
	tripQueueCollection   = "trip_queue"
	timeoutQuery          = time.Second * 10
	collectionCreatedCode = 48
)

var _ repository.TripQueueRepo = (*TripQueueRepositoryImpl)(nil)

type TripQueueRepositoryImpl struct {
	logger *slog.Logger
	db     *mongo.Database
}

func (t TripQueueRepositoryImpl) Clear() error {
	ctx, cancel := context.WithTimeout(context.Background(), timeoutQuery)
	defer cancel()

	filter := bson.M{}

	_, err := t.db.Collection(tripQueueCollection).DeleteMany(ctx, filter)

	if err != nil {
		return fmt.Errorf("postgres.Clear.DeleteMany::%w", err)
	}

	return nil
}

func (t TripQueueRepositoryImpl) GetTripForDriver(ctx context.Context, driverID string) ([]string, error) {
	ctx, cancel := context.WithTimeout(ctx, timeoutQuery)
	defer cancel()

	filter := bson.M{
		"driver_id": driverID,
	}
	var response []string

	cursor, err := t.db.Collection(tripQueueCollection).Find(ctx, filter)

	if err != nil && !errors.Is(err, mongo.ErrNoDocuments) {
		return nil, fmt.Errorf("mongodb.GetTripForDriver.Find::%w", err)
	}

	for cursor.Next(ctx) {
		var elem model.DriverTrip
		err := cursor.Decode(&elem)
		if err != nil {
			return nil, fmt.Errorf("mongodb.GetTripForDriver.Decode::%w", err)
		}
		response = append(response, elem.TripID)
	}

	return response, nil
}

func (t TripQueueRepositoryImpl) DeleteTrip(ctx context.Context, tripID string) error {
	ctx, cancel := context.WithTimeout(ctx, timeoutQuery)
	defer cancel()

	filter := bson.M{
		"trip_id": tripID,
	}

	_, err := t.db.Collection(tripQueueCollection).DeleteMany(ctx, filter)

	if err != nil {
		return fmt.Errorf("mongodb.DeleteTrip.DeleteMany::%w", err)
	}

	return nil
}

func (t TripQueueRepositoryImpl) InsertPairs(ctx context.Context, pairs []model.DriverTrip) error {
	ctx, cancel := context.WithTimeout(ctx, timeoutQuery)
	defer cancel()
	newValue := make([]interface{}, len(pairs))

	for i := range pairs {
		newValue[i] = pairs[i]
	}

	if len(newValue) != 0 {
		_, err := t.db.Collection(tripQueueCollection).InsertMany(ctx, newValue)
		if err != nil {
			return fmt.Errorf("mongodb.InsertPairs.InsertMany::%w", err)
		}
	}

	return nil
}

func CreateTripQueueRepo(logger *slog.Logger, db *mongo.Database) (*TripQueueRepositoryImpl, error) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	if db != nil {
		err := db.CreateCollection(ctx, tripQueueCollection)
		if err != nil {
			var we mongo.CommandError
			if errors.As(err, &we) {
				if we.Code == collectionCreatedCode {
					err = nil
				}
			}

			if err != nil {
				return nil, fmt.Errorf("mongodb.CreateTripQueueRepo::%w", err)
			}
		}
	}

	return &TripQueueRepositoryImpl{
		logger: logger,
		db:     db,
	}, nil
}
