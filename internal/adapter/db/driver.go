package db

import (
	"context"
	"errors"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"log/slog"
	"taxi_services/internal/core/model"
	"taxi_services/internal/ports/repository"
	"time"
)

const (
	driverCollection = "driver"
)

var _ repository.DriverRepo = (*DriverRepositoryImpl)(nil)

type DriverRepositoryImpl struct {
	logger *slog.Logger
	db     *mongo.Database
}

func (d DriverRepositoryImpl) Clear() error {
	ctx, cancel := context.WithTimeout(context.Background(), timeoutQuery)
	defer cancel()

	filter := bson.M{}

	_, err := d.db.Collection(driverCollection).DeleteMany(ctx, filter)

	if err != nil {
		return fmt.Errorf("mongodb.Clear:%w", err)
	}

	return nil
}

func (d DriverRepositoryImpl) GetTrip(ctx context.Context, tripID string) (model.Trip, error) {
	ctx, cancel := context.WithTimeout(ctx, timeoutQuery)
	defer cancel()

	filter := bson.M{
		"trip_id": tripID,
	}
	response := model.Trip{}

	err := d.db.Collection(driverCollection).FindOne(ctx, filter).Decode(&response)

	if err != nil {
		return response, fmt.Errorf("mongodb.GetTrip.Decode:%w", err)
	}

	return response, nil
}

func (d DriverRepositoryImpl) GetTripDriver(ctx context.Context, tripID string, driverID string) (model.Trip, error) {
	ctx, cancel := context.WithTimeout(ctx, timeoutQuery)
	defer cancel()

	filter := bson.M{
		"trip_id":   tripID,
		"driver_id": driverID,
	}
	var response model.Trip

	err := d.db.Collection(driverCollection).FindOne(ctx, filter).Decode(&response)

	if err != nil {
		return response, fmt.Errorf("mongodb.GetTripDriver.Decode:%w", err)
	}

	return response, nil
}

func (d DriverRepositoryImpl) InsertTrip(ctx context.Context, trip model.Trip) error {
	ctx, cancel := context.WithTimeout(ctx, timeoutQuery)
	defer cancel()

	filter := bson.M{
		"trip_id": trip.TripID,
	}

	result := model.Trip{}

	err := d.db.Collection(driverCollection).FindOne(ctx, filter).Decode(&result)

	if !errors.Is(err, mongo.ErrNoDocuments) {
		return fmt.Errorf("mongodb: already have trip with this id")
	}

	_, err = d.db.Collection(driverCollection).InsertOne(ctx, trip)

	if err != nil {
		return fmt.Errorf("mongodb.InsertTrip:%w", err)
	}

	return nil
}

func (d DriverRepositoryImpl) DriverCancelTrip(ctx context.Context, tripID string, driverID string) error {
	ctx, cancel := context.WithTimeout(ctx, timeoutQuery)
	defer cancel()

	filter := bson.M{
		"trip_id":   tripID,
		"driver_id": driverID,
	}

	update := bson.D{
		{Key: "$set", Value: bson.D{
			{Key: "status", Value: model.TripStatusCanceled},
		}},
	}

	res, err := d.db.Collection(driverCollection).UpdateOne(ctx, filter, update)
	if err != nil {
		return fmt.Errorf("mongodb.DriverCancelTrip::%w", err)
	}
	if res.MatchedCount == 0 {
		return fmt.Errorf("mongodb.DriverCancelTrip: no matching trips")
	}

	return nil
}

func (d DriverRepositoryImpl) ClientCancelTrip(ctx context.Context, tripID string) error {
	ctx, cancel := context.WithTimeout(ctx, timeoutQuery)
	defer cancel()

	filter := bson.M{
		"trip_id": tripID,
	}

	update := bson.D{
		{Key: "$set", Value: bson.D{
			{Key: "status", Value: model.TripStatusCanceled},
		}},
	}

	res, err := d.db.Collection(driverCollection).UpdateOne(ctx, filter, update)
	if err != nil {
		return fmt.Errorf("mongodb.ClientCancelTrip::%w", err)
	}
	if res.MatchedCount == 0 {
		return fmt.Errorf("mongodb.ClientCancelTrip: no matching trips")
	}

	return nil
}

func (d DriverRepositoryImpl) EndTrip(ctx context.Context, tripID string, driverID string) error {
	ctx, cancel := context.WithTimeout(ctx, timeoutQuery)
	defer cancel()

	filter := bson.M{
		"trip_id": tripID,
		"status": bson.M{
			"$eq": model.TripStatusStarted,
		},
		"driver_id": driverID,
	}

	update := bson.D{
		{Key: "$set", Value: bson.D{
			{Key: "status", Value: model.TripStatusEnded},
		}},
	}

	res, err := d.db.Collection(driverCollection).UpdateOne(ctx, filter, update)
	if err != nil {
		return fmt.Errorf("mongodb.EndTrip::%w", err)
	}
	if res.MatchedCount == 0 {
		return fmt.Errorf("mongodb.EndTrip: no matching trips")
	}

	return nil
}

func (d DriverRepositoryImpl) StartTrip(ctx context.Context, tripID string, driverID string) error {
	ctx, cancel := context.WithTimeout(ctx, timeoutQuery)
	defer cancel()

	filter := bson.M{
		"trip_id": tripID,
		"status": bson.M{
			"$eq": model.TripStatusFound,
		},
	}

	update := bson.D{
		{Key: "$set", Value: bson.D{
			{Key: "status", Value: model.TripStatusStarted},
		}},
		{Key: "$set", Value: bson.D{
			{Key: "driver_id", Value: driverID},
		}},
	}
	res, err := d.db.Collection(driverCollection).UpdateOne(ctx, filter, update)
	if err != nil {
		return fmt.Errorf("mongodb.StartTrip::%w", err)
	}
	if res.MatchedCount == 0 {
		return fmt.Errorf("mongodb.StartTrip: no matching trips")
	}

	return nil
}

func (d DriverRepositoryImpl) AcceptTrip(ctx context.Context, tripID string, driverID string) error {
	ctx, cancel := context.WithTimeout(ctx, timeoutQuery)
	defer cancel()

	filter := bson.M{
		"trip_id": tripID,
		"status": bson.M{
			"$eq": model.TripStatusSearch,
		},
	}

	update := bson.D{
		{Key: "$set", Value: bson.D{
			{Key: "status", Value: model.TripStatusFound},
		}},
		{Key: "$set", Value: bson.D{
			{Key: "driver_id", Value: driverID},
		}},
	}

	res, err := d.db.Collection(driverCollection).UpdateOne(ctx, filter, update)
	if err != nil {
		return fmt.Errorf("mongodb.AcceptTrip::%w", err)
	}
	if res.MatchedCount == 0 {
		return fmt.Errorf("mongodb.AcceptTrip: no matching trips")
	}

	return nil
}

func CreateDriverRepo(logger *slog.Logger, db *mongo.Database) (*DriverRepositoryImpl, error) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	if db != nil {
		err := db.CreateCollection(ctx, driverCollection)
		if err != nil {
			var we mongo.CommandError
			if errors.As(err, &we) {
				if we.Code == collectionCreatedCode {
					err = nil
				}
			}

			if err != nil {
				return nil, fmt.Errorf("mongodb.CreateDriverRepo::%w", err)
			}
		}
	}

	return &DriverRepositoryImpl{
		logger: logger,
		db:     db,
	}, nil
}
