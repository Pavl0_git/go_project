package db

import (
	"context"
	"fmt"
	"github.com/jmoiron/sqlx"
	"log/slog"
	model "taxi_services/internal/core/model"
	"taxi_services/internal/ports/repository"
)

var _ repository.LocationRepo = (*LocationRepoImpl)(nil)

type LocationRepoImpl struct {
	logger *slog.Logger
	db     *sqlx.DB
}

func (l LocationRepoImpl) GetDrivers(ctx context.Context, location model.Location, radius float32) ([]model.Driver, error) {
	ctx, cancel := context.WithTimeout(ctx, timeoutQuery)
	defer cancel()

	var drivers []model.Driver

	rows, err := l.db.Query(`
		SELECT lat, lng, id
			FROM "location"
		WHERE earth_distance(
			ll_to_earth(lat, lng),
			ll_to_earth($1, $2)
				) <= $3;`,
		location.Latitude, location.Longitude, radius)

	if err != nil {
		return nil, fmt.Errorf("postgres.GetDrivers::%w", err)
	}

	for rows.Next() {
		var driver model.Driver

		err := rows.Scan(&driver.Latitude, &driver.Longitude, &driver.DriverID)
		if err != nil {
			return nil, fmt.Errorf("postgres.GetDrivers.Scan::%w", err)
		}
		drivers = append(drivers, driver)
	}

	return drivers, nil

}

func (l LocationRepoImpl) ChangeDriverLocation(ctx context.Context, driverID string, location model.Location) error {
	ctx, cancel := context.WithTimeout(ctx, timeoutQuery)
	defer cancel()

	tx, err := l.db.Begin()
	if err != nil {
		return fmt.Errorf("postgres.ChangeDriverLocation.Begin::%w", err)
	}

	var cnt int

	err = l.db.QueryRow(`SELECT count(id)
		FROM "location" WHERE id = $1`, driverID).Scan(
		&cnt)

	if err != nil {
		return fmt.Errorf("postgres.ChangeDriverLocation.QueryRow::%w", err)
	}

	if cnt > 0 {
		_, err = tx.Exec(
			`UPDATE "location"
			SET lat = $1, lng=$2
				WHERE id = $3`,
			location.Latitude, location.Longitude, driverID)
		if err != nil {
			tx.Rollback()
			return fmt.Errorf("postgres.ChangeDriverLocation.Exec::%w", err)
		}
		err = tx.Commit()
		if err != nil {
			tx.Rollback()
			return fmt.Errorf("postgres.ChangeDriverLocation.Commit::%w", err)
		}
	} else {
		_, err = tx.Query(`
		INSERT INTO "location"(id, lat, lng)
			VALUES ($1,$2,$3)`,
			driverID, location.Latitude, location.Longitude)

		if err != nil {
			tx.Rollback()
			return fmt.Errorf("postgres.ChangeDriverLocation.Query::%w", err)
		}
		tx.Commit()
		if err != nil {
			tx.Rollback()
			return fmt.Errorf("postgres.ChangeDriverLocation.Commit::%w", err)
		}
	}

	return nil
}

func (l LocationRepoImpl) Clear() error {
	tx, err := l.db.Begin()
	if err != nil {
		return err
	}
	_, err = tx.Exec(
		`DELETE FROM "location"`,
	)
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("postgres.Clear.Exec::%w", err)
	}
	err = tx.Commit()
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("postgres.Clear.Commit::%w", err)
	}
	return nil
}

func CreateLocationRepo(logger *slog.Logger, db *sqlx.DB) (*LocationRepoImpl, error) {
	return &LocationRepoImpl{logger: logger, db: db}, nil
}
