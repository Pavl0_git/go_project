package location

import (
	"context"
	"fmt"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"github.com/pressly/goose/v3"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
	"github.com/testcontainers/testcontainers-go"
	"github.com/testcontainers/testcontainers-go/wait"
	"log/slog"
	"os"
	"taxi_services/internal/adapter/db"
	"taxi_services/internal/adapter/rest"
	"taxi_services/internal/core/model"
	"taxi_services/internal/core/services"
	"taxi_services/internal/ports"
	"taxi_services/pkg/config"
	"testing"
	"time"
)

func TestTestcontainers(t *testing.T) {
	suite.Run(t, new(TestcontainersSuite))
}

type TestcontainersSuite struct {
	suite.Suite

	locationSvc ports.LocationSvc
	pgContainer testcontainers.Container
}

func (s *TestcontainersSuite) TestLocationSvcImpl_GetDrivers() {
	ctx := context.Background()

	cases := []struct {
		name            string
		input_drivers   []model.Driver
		expect_drivers  []model.Driver
		search_location model.Location
		radius          float32
	}{
		{
			"Test 1",
			[]model.Driver{
				{
					DriverID:  "1",
					Longitude: 55.748025,
					Latitude:  37.539906,
				},
				{
					DriverID:  "2",
					Longitude: 55.750332,
					Latitude:  37.583957,
				},
				{
					DriverID:  "3",
					Longitude: 55.794615,
					Latitude:  37.676504,
				},
			},
			[]model.Driver{
				{
					DriverID:  "2",
					Longitude: 55.750332,
					Latitude:  37.583957,
				},
			},
			model.Location{
				Longitude: 55.754721,
				Latitude:  37.571855,
			},
			2500.0,
		},
		{
			"Test 2",
			[]model.Driver{
				{
					DriverID:  "1",
					Longitude: 55.748025,
					Latitude:  37.539906,
				},
				{
					DriverID:  "2",
					Longitude: 55.750332,
					Latitude:  37.583957,
				},
				{
					DriverID:  "3",
					Longitude: 55.794615,
					Latitude:  37.676504,
				},
			},
			[]model.Driver{
				{
					DriverID:  "1",
					Longitude: 55.748025,
					Latitude:  37.539906,
				},
				{
					DriverID:  "2",
					Longitude: 55.750332,
					Latitude:  37.583957,
				},
			},
			model.Location{
				Longitude: 55.754721,
				Latitude:  37.571855,
			},
			4000.0,
		},
		{
			"Test 3",
			[]model.Driver{
				{
					DriverID:  "1",
					Longitude: 55.748025,
					Latitude:  37.539906,
				},
				{
					DriverID:  "2",
					Longitude: 55.750332,
					Latitude:  37.583957,
				},
				{
					DriverID:  "3",
					Longitude: 55.794615,
					Latitude:  37.676504,
				},
			},
			[]model.Driver{
				{
					DriverID:  "1",
					Longitude: 55.748025,
					Latitude:  37.539906,
				},
				{
					DriverID:  "2",
					Longitude: 55.750332,
					Latitude:  37.583957,
				},
				{
					DriverID:  "3",
					Longitude: 55.794615,
					Latitude:  37.676504,
				},
			},
			model.Location{
				Longitude: 55.754721,
				Latitude:  37.571855,
			},
			20000.0,
		},
		{
			"Test 5",
			[]model.Driver{
				{
					DriverID:  "1",
					Longitude: 55.748025,
					Latitude:  37.539906,
				},
				{
					DriverID:  "1",
					Longitude: 55.750332,
					Latitude:  37.583957,
				},
			},
			[]model.Driver{
				{
					DriverID:  "1",
					Longitude: 55.750332,
					Latitude:  37.583957,
				},
			},
			model.Location{
				Longitude: 55.754721,
				Latitude:  37.571855,
			},
			2500.0,
		},
	}

	for _, cs := range cases {
		for _, driver := range cs.input_drivers {
			err := s.locationSvc.ChangeDriverLocation(ctx, driver.DriverID, model.Location{
				Latitude:  driver.Latitude,
				Longitude: driver.Longitude,
			})
			s.Require().NoError(err)
		}
		s.T().Run(cs.name, func(t *testing.T) {
			res, err := s.locationSvc.GetDrivers(ctx, cs.search_location, cs.radius)
			assert.Empty(t, err)
			assert.Equal(t, cs.expect_drivers, res)
		})
		err := s.locationSvc.Clear()
		s.Require().NoError(err)
	}

}

func (suite *TestcontainersSuite) SetupSuite() {
	textHandler := slog.NewTextHandler(os.Stdout, nil)
	log := slog.New(textHandler)
	ctx := context.Background()

	dbContainer, err := testcontainers.GenericContainer(ctx, testcontainers.GenericContainerRequest{
		ContainerRequest: testcontainers.ContainerRequest{
			Image:        "postgres",
			ExposedPorts: []string{"5432"},
			Env: map[string]string{
				"POSTGRES_DB":       "location",
				"POSTGRES_USER":     "postgres",
				"POSTGRES_PASSWORD": "postgres",
			},
			WaitingFor: wait.ForLog("database system is ready to accept connections"),
		},
		Started: true,
	})
	suite.Require().NoError(err)

	// with a second delay migrations work properly
	time.Sleep(time.Second * 5)

	ip, err := dbContainer.Host(ctx)
	suite.Require().NoError(err)
	port, err := dbContainer.MappedPort(ctx, "5432")
	suite.Require().NoError(err)

	DSN := fmt.Sprintf(`postgres://%s:%s@%s:%d/%s?sslmode=%s`,
		"postgres",
		"postgres",
		ip,
		uint16(port.Int()),
		"location",
		"disable",
	)

	cfg := config.Config{
		LocationServer: config.LocationServerConfig{
			HttpServerPort: 8081,
		},
		LocationPostgres: config.LocationPostgresConfig{
			Database: "location",
			DSN:      DSN,
		},
		LocationMigrations: config.LocationMigrationsConfig{
			Path:    "../../../migrations/location",
			Enabled: "true",
		},
	}

	database, err := sqlx.Open("postgres", cfg.LocationPostgres.DSN)
	suite.Require().NoError(err)
	err = database.PingContext(ctx)
	suite.Require().NoError(err)

	if cfg.LocationMigrations.Enabled == "true" {
		fs := os.DirFS(cfg.LocationMigrations.Path)
		goose.SetBaseFS(fs)
		err = goose.SetDialect("postgres")
		suite.Require().NoError(err)
		err = goose.UpContext(ctx, database.DB, ".")
		suite.Require().NoError(err)
	}

	locationRepo, err := db.CreateLocationRepo(log, database)
	suite.Require().NoError(err)

	locationService := services.NewLocationSvcImpl(log, locationRepo)

	restServer := rest.NewLocationServer(log, &cfg, locationService)
	restServer.SetUp()

	go func() {
		err = restServer.Start()
		suite.Require().NoError(err)
	}()

	suite.locationSvc = locationService
	suite.pgContainer = dbContainer
}

func (s *TestcontainersSuite) TearDownSuite() {
	s.pgContainer.Terminate(context.Background())
	s.T().Log("Suite stop is done")
}
