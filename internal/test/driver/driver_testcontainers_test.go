package test

import (
	"context"
	"fmt"
	"github.com/golang/mock/gomock"
	_ "github.com/lib/pq"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
	"github.com/testcontainers/testcontainers-go"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
	"log/slog"
	"os"
	"taxi_services/internal/adapter/db"
	"taxi_services/internal/adapter/rest"
	"taxi_services/internal/core/model"
	"taxi_services/internal/core/services"
	"taxi_services/internal/ports"
	mock_ports "taxi_services/internal/ports/mocks"
	"taxi_services/pkg/config"
	"taxi_services/pkg/migration"
	"testing"
	"time"
)

func TestTestcontainers(t *testing.T) {
	suite.Run(t, new(TestcontainersSuite))
}

type TestcontainersSuite struct {
	suite.Suite

	driverSvc   ports.DriverSvc
	pgContainer testcontainers.Container
}

func (s *TestcontainersSuite) TestDriverSvcImpl_GetTripForDriver() {
	ctx := context.Background()

	cases := []struct {
		name           string
		driver         model.Driver
		trips          []model.Trip
		near           []bool
		expected_trips []model.Trip
	}{
		{
			name: "Test 1",
			driver: model.Driver{
				DriverID:  "1",
				Latitude:  90,
				Longitude: 90,
			},
			trips: []model.Trip{
				{
					"10",
					"",
					model.Location{
						Latitude:  89,
						Longitude: 90,
					},
					model.Location{
						Latitude:  0,
						Longitude: 0,
					},
					model.Price{
						Amount:   10,
						Currency: "RUB",
					},
					"DRIVER_SEARCH",
				},
				{
					"11",
					"",
					model.Location{
						Latitude:  0,
						Longitude: 0,
					},
					model.Location{
						Latitude:  0,
						Longitude: 0,
					},
					model.Price{
						Amount:   10,
						Currency: "RUB",
					},
					"DRIVER_SEARCH",
				},
				{
					"12",
					"",
					model.Location{
						Latitude:  60,
						Longitude: 120,
					},
					model.Location{
						Latitude:  0,
						Longitude: 0,
					},
					model.Price{
						Amount:   10,
						Currency: "RUB",
					},
					"DRIVER_SEARCH",
				},
			},
			near: []bool{
				true,
				false,
				false,
			},
			expected_trips: []model.Trip{
				{
					"10",
					"",
					model.Location{
						Latitude:  89,
						Longitude: 90,
					},
					model.Location{
						Latitude:  0,
						Longitude: 0,
					},
					model.Price{
						Amount:   10,
						Currency: "RUB",
					},
					"DRIVER_SEARCH",
				},
			},
		},
		{
			name: "Test 2",
			driver: model.Driver{
				DriverID:  "1",
				Latitude:  90,
				Longitude: 90,
			},
			trips: []model.Trip{
				{
					"10",
					"",
					model.Location{
						Latitude:  90,
						Longitude: 90,
					},
					model.Location{
						Latitude:  0,
						Longitude: 0,
					},
					model.Price{
						Amount:   10,
						Currency: "RUB",
					},
					"DRIVER_SEARCH",
				},
				{
					"11",
					"",
					model.Location{
						Latitude:  90,
						Longitude: 90,
					},
					model.Location{
						Latitude:  0,
						Longitude: 0,
					},
					model.Price{
						Amount:   10,
						Currency: "RUB",
					},
					"DRIVER_SEARCH",
				},
				{
					"12",
					"",
					model.Location{
						Latitude:  90,
						Longitude: 90,
					},
					model.Location{
						Latitude:  0,
						Longitude: 0,
					},
					model.Price{
						Amount:   10,
						Currency: "RUB",
					},
					"DRIVER_SEARCH",
				},
			},
			near: []bool{
				true,
				true,
				true,
			},
			expected_trips: []model.Trip{
				{
					"10",
					"",
					model.Location{
						Latitude:  90,
						Longitude: 90,
					},
					model.Location{
						Latitude:  0,
						Longitude: 0,
					},
					model.Price{
						Amount:   10,
						Currency: "RUB",
					},
					"DRIVER_SEARCH",
				},
				{
					"11",
					"",
					model.Location{
						Latitude:  90,
						Longitude: 90,
					},
					model.Location{
						Latitude:  0,
						Longitude: 0,
					},
					model.Price{
						Amount:   10,
						Currency: "RUB",
					},
					"DRIVER_SEARCH",
				},
				{
					"12",
					"",
					model.Location{
						Latitude:  90,
						Longitude: 90,
					},
					model.Location{
						Latitude:  0,
						Longitude: 0,
					},
					model.Price{
						Amount:   10,
						Currency: "RUB",
					},
					"DRIVER_SEARCH",
				},
			},
		},
	}

	for _, cs := range cases {
		ctrl := gomock.NewController(s.T())
		locationService := mock_ports.NewMockLocationSvc(ctrl)
		cfg := config.Config{
			LocationServer: config.LocationServerConfig{
				HttpServerPort: 8088,
			},
		}
		restLocationServer := rest.NewLocationServer(slog.Default(), &cfg, locationService)
		restLocationServer.SetUp()

		go func() {
			restLocationServer.Start()
		}()

		for i, trip := range cs.trips {
			if cs.near[i] == true {
				locationService.EXPECT().GetDrivers(gomock.Any(), trip.StartLocation, float32(2000.0)).Return([]model.Driver{cs.driver}, nil)
			} else {
				locationService.EXPECT().GetDrivers(gomock.Any(), trip.StartLocation, float32(2000.0)).Return([]model.Driver{}, nil)
			}
			event := model.CreatedEvent{
				Data: model.CreatedData{
					TripID:        trip.TripID,
					OfferID:       "",
					Price:         trip.Price,
					Status:        trip.Status,
					StartLocation: trip.StartLocation,
					FinalLocation: trip.FinalLocation,
				},
			}
			err := s.driverSvc.InsertNewTrip(ctx, event)
			s.Require().NoError(err)
		}
		s.T().Run(cs.name, func(t *testing.T) {
			res, err := s.driverSvc.GetTripForDriver(ctx, cs.driver.DriverID)
			assert.Empty(t, err)
			assert.Equal(t, cs.expected_trips, res)
		})
		err := s.driverSvc.Clear()
		s.Require().NoError(err)

		_ = restLocationServer.Stop(ctx)
		ctrl.Finish()
	}

}

func (s *TestcontainersSuite) TestComplexScenario() {
	ctx := context.Background()

	cases := []struct {
		name           string
		drivers        []model.Driver
		trips          []model.Trip
		near           []bool
		expected_trips []model.Trip
	}{
		{
			name: "Test 1",
			drivers: []model.Driver{
				{
					DriverID:  "1",
					Latitude:  89,
					Longitude: 90,
				},
				{
					DriverID:  "2",
					Latitude:  91,
					Longitude: 90,
				},
			},
			trips: []model.Trip{
				{
					"10",
					"",
					model.Location{
						Latitude:  90,
						Longitude: 90,
					},
					model.Location{
						Latitude:  0,
						Longitude: 0,
					},
					model.Price{
						Amount:   10,
						Currency: "RUB",
					},
					"DRIVER_SEARCH",
				},
				{
					"11",
					"",
					model.Location{
						Latitude:  88,
						Longitude: 90,
					},
					model.Location{
						Latitude:  0,
						Longitude: 0,
					},
					model.Price{
						Amount:   10,
						Currency: "RUB",
					},
					"DRIVER_SEARCH",
				},
			},
			expected_trips: []model.Trip{
				{
					"10",
					"",
					model.Location{
						Latitude:  89,
						Longitude: 90,
					},
					model.Location{
						Latitude:  0,
						Longitude: 0,
					},
					model.Price{
						Amount:   10,
						Currency: "RUB",
					},
					"DRIVER_SEARCH",
				},
			},
		},
	}

	for _, cs := range cases {
		ctrl := gomock.NewController(s.T())
		locationService := mock_ports.NewMockLocationSvc(ctrl)
		cfg := config.Config{
			LocationServer: config.LocationServerConfig{
				HttpServerPort: 8088,
			},
		}
		restLocationServer := rest.NewLocationServer(slog.Default(), &cfg, locationService)
		restLocationServer.SetUp()

		go func() {
			restLocationServer.Start()
		}()

		locationService.EXPECT().GetDrivers(gomock.Any(), cs.trips[0].StartLocation, float32(2000.0)).Return([]model.Driver{cs.drivers[0], cs.drivers[1]}, nil)
		locationService.EXPECT().GetDrivers(gomock.Any(), cs.trips[1].StartLocation, float32(2000.0)).Return([]model.Driver{cs.drivers[0]}, nil)
		for _, trip := range cs.trips {
			event := model.CreatedEvent{
				Data: model.CreatedData{
					TripID:        trip.TripID,
					OfferID:       "",
					Price:         trip.Price,
					Status:        trip.Status,
					StartLocation: trip.StartLocation,
					FinalLocation: trip.FinalLocation,
				},
			}
			err := s.driverSvc.InsertNewTrip(ctx, event)
			s.Require().NoError(err)
		}
		s.T().Run(cs.name, func(t *testing.T) {
			res, err := s.driverSvc.GetTripForDriver(ctx, cs.drivers[1].DriverID)
			assert.Empty(t, err)
			assert.Equal(t, []model.Trip{cs.trips[0]}, res)
		})
		err := s.driverSvc.AcceptTrip(ctx, cs.trips[0].TripID, cs.drivers[1].DriverID)
		s.Require().NoError(err)
		s.T().Run(cs.name, func(t *testing.T) {
			res, err := s.driverSvc.GetTripForDriver(ctx, cs.drivers[0].DriverID)
			assert.Empty(t, err)
			assert.Equal(t, []model.Trip{cs.trips[1]}, res)
		})
		err = s.driverSvc.Clear()
		s.Require().NoError(err)

		_ = restLocationServer.Stop(ctx)
		ctrl.Finish()
	}

}

func (s *TestcontainersSuite) TestOneEndToEndTrip() {
	ctx := context.Background()

	trip := model.Trip{
		"10",
		"",
		model.Location{
			Latitude:  89,
			Longitude: 90,
		},
		model.Location{
			Latitude:  0,
			Longitude: 0,
		},
		model.Price{
			Amount:   10,
			Currency: "RUB",
		},
		"DRIVER_SEARCH",
	}

	driver := model.Driver{
		DriverID:  "1",
		Latitude:  89,
		Longitude: 90,
	}

	ctrl := gomock.NewController(s.T())
	locationService := mock_ports.NewMockLocationSvc(ctrl)
	cfg := config.Config{
		LocationServer: config.LocationServerConfig{
			HttpServerPort: 8088,
		},
	}
	restLocationServer := rest.NewLocationServer(slog.Default(), &cfg, locationService)
	restLocationServer.SetUp()

	go func() {
		restLocationServer.Start()
	}()

	locationService.EXPECT().GetDrivers(gomock.Any(), trip.StartLocation, float32(2000.0)).Return([]model.Driver{driver}, nil)
	event := model.CreatedEvent{
		Data: model.CreatedData{
			TripID:        trip.TripID,
			OfferID:       "",
			Price:         trip.Price,
			Status:        trip.Status,
			StartLocation: trip.StartLocation,
			FinalLocation: trip.FinalLocation,
		},
	}
	err := s.driverSvc.InsertNewTrip(ctx, event)
	s.Require().NoError(err)
	s.T().Run("Test getting trip", func(t *testing.T) {
		res, err := s.driverSvc.GetTripForDriver(ctx, driver.DriverID)
		assert.Empty(t, err)
		assert.Equal(t, []model.Trip{trip}, res)
	})
	err = s.driverSvc.AcceptTrip(ctx, trip.TripID, driver.DriverID)
	s.Require().NoError(err)
	err = s.driverSvc.StartTrip(ctx, trip.TripID, driver.DriverID)
	s.Require().NoError(err)
	err = s.driverSvc.AcceptTrip(ctx, trip.TripID, driver.DriverID)
	s.Require().Error(err)
	err = s.driverSvc.EndTrip(ctx, trip.TripID, driver.DriverID)
	s.Require().NoError(err)

	err = s.driverSvc.Clear()
	s.Require().NoError(err)

	_ = restLocationServer.Stop(ctx)
	ctrl.Finish()
}

func (suite *TestcontainersSuite) SetupSuite() {
	textHandler := slog.NewTextHandler(os.Stdout, nil)
	log := slog.New(textHandler)
	ctx := context.Background()

	dbContainer, err := testcontainers.GenericContainer(ctx, testcontainers.GenericContainerRequest{
		ContainerRequest: testcontainers.ContainerRequest{
			Image:        "mongo",
			ExposedPorts: []string{"27017"},
			//WaitingFor:   wait.ForLog("waiting for connections"),
		},
		Started: true,
	})
	suite.Require().NoError(err)

	// with a second delay migrations work properly
	time.Sleep(time.Second * 5)

	ip, err := dbContainer.Host(ctx)
	suite.Require().NoError(err)
	port, err := dbContainer.MappedPort(ctx, "27017")
	suite.Require().NoError(err)
	URI := fmt.Sprintf(`mongodb://%s:%d/%s`,
		ip,
		uint16(port.Int()),
		"driver",
	)

	cfg := config.Config{
		DriverServer: config.DriverServerConfig{
			HttpServerPort: 8081,
		},
		DriverMongo: config.DriverMongoConfig{
			Database: "driver",
			Uri:      URI,
		},
		DriverMigrations: config.DriverMigrationsConfig{
			Path:    "../../../migrations/driver",
			Enabled: "true",
		},
		LocationServer: config.LocationServerConfig{
			HttpServerPort: 8088,
		},
	}

	client, err := mongo.Connect(ctx, options.Client().ApplyURI(cfg.DriverMongo.Uri))
	suite.Require().NoError(err)
	err = client.Ping(ctx, readpref.Primary())
	suite.Require().NoError(err)

	database := client.Database(cfg.DriverMongo.Database)

	if cfg.DriverMigrations.Enabled == "true" {
		migrationSvc := migration.NewMigrationsService(log, database)
		err = migrationSvc.RunMigrations(cfg.DriverMigrations.Path)
		suite.Require().NoError(err)
	}

	driverRepo, err := db.CreateDriverRepo(log, database)
	suite.Require().NoError(err)
	tripQueueRepo, err := db.CreateTripQueueRepo(log, database)
	suite.Require().NoError(err)

	reg := prometheus.NewRegistry()

	driverService := services.NewDriverSvcImpl(log, services.KafkaInteractors{}, driverRepo, tripQueueRepo, &cfg, reg)

	restServer := rest.NewDriverServer(log, &cfg, driverService, reg)
	restServer.SetUp()

	go func() {
		err = restServer.Start()
		suite.Require().NoError(err)
	}()

	suite.driverSvc = driverService
	suite.pgContainer = dbContainer
}

func (s *TestcontainersSuite) TearDownSuite() {
	s.pgContainer.Terminate(context.Background())
	s.T().Log("Suite stop is done")
}
