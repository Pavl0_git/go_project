package internal

import (
	"context"
	"fmt"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"github.com/pressly/goose/v3"
	"log/slog"
	"os"
	"taxi_services/internal/adapter/db"
	"taxi_services/internal/adapter/rest"
	"taxi_services/internal/core/services"
	"taxi_services/pkg/config"
	"time"
)

const driverName = "postgres"

type LocationApplication struct {
	log        *slog.Logger
	cfg        *config.Config
	restServer *rest.LocationServer
}

func NewLocationApplication(log *slog.Logger, conf *config.Config) *LocationApplication {
	return &LocationApplication{log: log, cfg: conf}
}

func (a *LocationApplication) Run(ctx context.Context) error {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	a.log.Info("Connecting to postgres...")
	database, err := sqlx.Open(driverName, a.cfg.LocationPostgres.DSN)
	if err != nil {
		return fmt.Errorf("unable to connect to database: %w", err)
	}

	database.DB.SetMaxOpenConns(100)
	database.DB.SetMaxIdleConns(10)
	database.DB.SetConnMaxLifetime(0)

	if err = database.PingContext(ctx); err != nil {
		return fmt.Errorf("database is not answering: %w", err)
	}
	if a.cfg.LocationMigrations.Enabled == "true" {
		a.log.Info("Handling migrations ...")
		fs := os.DirFS(a.cfg.LocationMigrations.Path)
		goose.SetBaseFS(fs)
		if err = goose.SetDialect(driverName); err != nil {
			return err
		}
		if err = goose.UpContext(ctx, database.DB, "."); err != nil {
			return err
		}
	}

	locationRepo, err := db.CreateLocationRepo(a.log, database)
	if err != nil {
		return fmt.Errorf("location repo create failed: %w", err)
	}

	locationService := services.NewLocationSvcImpl(a.log, locationRepo)

	a.restServer = rest.NewLocationServer(a.log, a.cfg, locationService)
	a.restServer.SetUp()

	go func() {
		if err = a.restServer.Start(); err != nil {
			a.log.Error(fmt.Sprintf("HTTP Server startup failed: %s", err.Error()))
		}
	}()

	return nil
}

func (a *LocationApplication) Stop() {
	ctx, cancel := context.WithTimeout(context.Background(), terminateTimeout)
	defer cancel()

	if a.restServer != nil {
		go func(ctx context.Context) {
			a.log.Debug("Shutting down HTTP restServer")
			if err := a.restServer.Stop(ctx); err != nil {
				a.log.Error(fmt.Sprintf("Failed to shut down HTTP Server in a proper way: %s", err.Error()))
			}
		}(ctx)
	}
}
