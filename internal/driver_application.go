package internal

import (
	"context"
	"fmt"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/segmentio/kafka-go"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log/slog"
	"taxi_services/internal/adapter/db"
	"taxi_services/internal/adapter/rest"
	"taxi_services/internal/core/services"
	"taxi_services/pkg/config"
	"taxi_services/pkg/migration"
	"time"
)

const (
	terminateTimeout = 2 * time.Minute
)

type DriverApplication struct {
	log         *slog.Logger
	cfg         *config.Config
	restServer  *rest.DriverServer
	mongoClient *mongo.Client
}

func NewDriverApplication(log *slog.Logger, conf *config.Config) *DriverApplication {
	return &DriverApplication{log: log, cfg: conf}
}

func (a *DriverApplication) Run(ctx context.Context) error {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	a.log.Info("Connecting to mongo...")
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(a.cfg.DriverMongo.Uri))
	if err != nil {
		return fmt.Errorf("new mongo client create error: %w", err)
	}

	err = client.Ping(ctx, nil)
	if err != nil {
		return fmt.Errorf("new mongo node connect error: %w", err)
	}

	a.mongoClient = client
	database := client.Database(a.cfg.DriverMongo.Database)

	if a.cfg.DriverMigrations.Enabled == "true" {
		a.log.Info("Handling migrations ...")
		migrationSvc := migration.NewMigrationsService(a.log, database)
		err = migrationSvc.RunMigrations(a.cfg.DriverMigrations.Path)
		if err != nil {
			return fmt.Errorf("run migrations failed")
		}
	}

	a.log.Info("Creating repositories...")
	driverRepo, err := db.CreateDriverRepo(a.log, database)
	if err != nil {
		return fmt.Errorf("driver repo create failed: %w", err)
	}
	tripQueueRepo, err := db.CreateTripQueueRepo(a.log, database)
	if err != nil {
		return fmt.Errorf("trip queue repo create failed: %w", err)
	}

	a.log.Info("Connecting to kafka...")
	kafkaCancelWriter := kafka.NewWriter(kafka.WriterConfig{
		Brokers:     []string{"kafka:9092"},
		Topic:       "command_cancel",
		Async:       true,
		Logger:      kafka.LoggerFunc(a.log.Info),
		ErrorLogger: kafka.LoggerFunc(a.log.Error),
	})

	kafkaEndWriter := kafka.NewWriter(kafka.WriterConfig{
		Brokers:     []string{"kafka:9092"},
		Topic:       "command_end",
		Async:       true,
		Logger:      kafka.LoggerFunc(a.log.Info),
		ErrorLogger: kafka.LoggerFunc(a.log.Error),
	})

	kafkaStartWriter := kafka.NewWriter(kafka.WriterConfig{
		Brokers:     []string{"kafka:9092"},
		Topic:       "command_start",
		Async:       true,
		Logger:      kafka.LoggerFunc(a.log.Info),
		ErrorLogger: kafka.LoggerFunc(a.log.Error),
	})

	kafkaAcceptWriter := kafka.NewWriter(kafka.WriterConfig{
		Brokers:     []string{"kafka:9092"},
		Topic:       "command_accept",
		Async:       true,
		Logger:      kafka.LoggerFunc(a.log.Info),
		ErrorLogger: kafka.LoggerFunc(a.log.Error),
	})

	kafkaCanceledReader := kafka.NewReader(kafka.ReaderConfig{
		Brokers:        []string{"kafka:9092"},
		Topic:          "event_canceled",
		GroupID:        "driver",
		SessionTimeout: time.Second * 6,
	})

	kafkaCreatedReader := kafka.NewReader(kafka.ReaderConfig{
		Brokers:        []string{"kafka:9092"},
		Topic:          "event_created",
		GroupID:        "driver",
		SessionTimeout: time.Second * 6,
	})

	kafkaInteractors := services.KafkaInteractors{
		CancelWriter: kafkaCancelWriter,
		EndWriter:    kafkaEndWriter,
		StartWriter:  kafkaStartWriter,
		AcceptWriter: kafkaAcceptWriter,
		CreateReader: kafkaCreatedReader,
		CancelReader: kafkaCanceledReader,
	}

	reg := prometheus.NewRegistry()

	a.log.Info("Starting service...")
	driverService := services.NewDriverSvcImpl(a.log, kafkaInteractors, driverRepo, tripQueueRepo, a.cfg, reg)

	a.restServer = rest.NewDriverServer(a.log, a.cfg, driverService, reg)
	a.restServer.SetUp()

	a.log.Info("Starting server...")
	go func() {
		if err = a.restServer.Start(); err != nil {
			a.log.Error(fmt.Sprintf("HTTP Server startup failed: %s", err.Error()))
		}
	}()

	return err
}

func (a *DriverApplication) Stop() {
	ctx, cancel := context.WithTimeout(context.Background(), terminateTimeout)
	defer cancel()

	if a.mongoClient != nil {
		go func(ctx context.Context) {
			a.log.Debug("Shutting down DB")
			if err := a.mongoClient.Disconnect(ctx); err != nil {
				a.log.Error(fmt.Sprintf("failed to shut down DB in a proper way: %s", err.Error()))
			}
		}(ctx)
	}

	if a.restServer != nil {
		go func(ctx context.Context) {
			a.log.Debug("Shutting down HTTP restServer")
			if err := a.restServer.Stop(ctx); err != nil {
				a.log.Error(fmt.Sprintf("failed to shut down HTTP Server in a proper way: %s", err.Error()))
			}
		}(ctx)
	}
}
