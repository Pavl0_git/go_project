package openapi

//go:generate oapi-codegen --package openapiDriver --config oapi-codegen.yaml -o ./driver/driver.gen.go ./driver/contract.yaml
//go:generate oapi-codegen --package openapiLocation --config oapi-codegen.yaml -o ./location/location.gen.go ./location/contract.yaml
