package main

import (
	"context"
	"log"
	"log/slog"
	"os"
	"taxi_services/internal"
	otel "taxi_services/internal/otel"
	"taxi_services/pkg/config"
	"taxi_services/pkg/shutdown"
	"time"
)

const (
	sleepTimeout = 3 * time.Second
)

func main() {
	cfg, err := config.NewConfig()
	if err != nil {
		log.Printf("failed to load config: %v", err)
	}

	textHandler := slog.NewTextHandler(os.Stdout, nil)
	log := slog.New(textHandler)

	app := internal.NewLocationApplication(log, cfg)
	shutdown.InitShutdown(log, app)

	otelShutdown := otel.InitProvider()
	defer otelShutdown()

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	shutdown.GC().Monitor(cancel)

	err = app.Run(ctx)
	if err != nil {
		log.With(slog.String("err_descr", err.Error())).Error("application Run() failed")
		shutdown.GC().Halt()
	} else {
		log.Info("server started")
	}

	shutdown.GC().Wait()
}
