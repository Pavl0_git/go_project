-- +goose Up
CREATE TABLE IF NOT EXISTS "location"
(
    lat float NOT NULL,
    lng float NOT NULL,
    id text NOT NULL
);

CREATE EXTENSION IF NOT EXISTS cube CASCADE;
CREATE EXTENSION IF NOT EXISTS earthdistance CASCADE;

-- +goose Down
DROP TABLE IF EXISTS location CASCADE;